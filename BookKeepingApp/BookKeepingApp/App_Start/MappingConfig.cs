﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookKeepingApp.App_Start
{
    internal static class MappingConfig
    {
        public static IMapper Initialize()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ExpenseService.Mapping.EntityMappingProfile());
            });

            return config.CreateMapper();
        }
    }
}