﻿using AppDataContracts.Core;
using AutoMapper;
using DataAccess;
using Microsoft.Practices.Unity;
using ServiceContracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace BookKeepingApp.App_Start
{
    public static class UnityConfig
    {
        #region Unity Container

        /// <summary>
        ///     Gets the configured Unity container.
        ///     This method is thread-safe and may be used concurrently from multiple threads.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return Container.Value;
        }

        #endregion

        #region Initialization

        /// <summary>
        ///     The Unity container with lazy initialization is thread-safe and may be used concurrently from multiple threads.
        /// </summary>
        private static readonly Lazy<IUnityContainer> Container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        ///     There is no need to register concrete types such as controllers or API controllers (unless you want to
        ///     change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.
        /// </remarks>
        private static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterDataContext();
            container.RegisterType<IMapper>(new ContainerControlledLifetimeManager(), new InjectionFactory((c) => MappingConfig.Initialize()));
            container.RegisterType<IExpenseService, ExpenseService.Service>(new PerResolveLifetimeManager());
            container.RegisterType<IPrincipalService, PrincipalService.Service>(new PerResolveLifetimeManager());
        }
        private static IUnityContainer RegisterDataContext(this IUnityContainer container)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            container.RegisterType<IDataContext, DataContext>();

            return container;
        }
        #endregion
    }
}