﻿using AppDataContracts;
using ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace BookKeepingApp.Controllers
{
    [Authorize]
    public class ExpenseController : Controller
    {
        private readonly IExpenseService _service;
        
        public ExpenseController(IExpenseService service)
        {
            _service = service;
        }
        // GET: Expense
        public ActionResult Index()
        {
            var result = _service.List();

            return View(result);
        }
        [HttpGet]
        public JsonResult List()
        {
            var result = _service.List();

            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult CategoryList()
        {
            var result = _service.GetCategoryList();

            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Add(AddExpenseDto model)
        {
            _service.AddExpense(model);
            return Json(new { });
        }
    }
}