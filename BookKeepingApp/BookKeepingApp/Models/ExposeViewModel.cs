﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookKeepingApp.Models
{
    public class ExposeViewModel
    {
        public decimal Amount { get; set; }
        public string CategoryName { get; set; }
        public int Month { get; set; }
    }
}