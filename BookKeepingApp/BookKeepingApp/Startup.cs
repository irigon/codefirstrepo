﻿using BookKeepingApp.App_Start;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BookKeepingApp.Startup))]
namespace BookKeepingApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            var container = UnityConfig.GetConfiguredContainer();
        }
    }
}
