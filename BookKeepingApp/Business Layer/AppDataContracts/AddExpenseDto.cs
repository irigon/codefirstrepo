﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDataContracts
{
    public class AddExpenseDto
    {
        public decimal Amount { get; set; }
        public int CategoryId { get; set; }
    }
}
