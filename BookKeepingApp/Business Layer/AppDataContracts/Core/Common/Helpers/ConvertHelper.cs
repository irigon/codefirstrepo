﻿using AutoMapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDataContracts.Core.Common.Helpers
{
    public static class ConvertHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="source"></param>
        /// <param name="mapper"></param>
        /// <returns></returns>
        public static List<TResult> ConvertToList<TSource, TResult>(this IEnumerable source, IMapper mapper)
            where TSource : class
        {
            var enumerable = source as IList<object> ?? source.Cast<object>().ToList();
            IEnumerable<TResult> ienumerableDest = mapper.Map<IEnumerable<TSource>, IEnumerable<TResult>>(enumerable.OfType<TSource>());
            return ienumerableDest.ToList();
        }
    }
}
