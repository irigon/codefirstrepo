﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AppDataContracts.Core
{
    public interface IDataContext
    {
        IQueryable<T> GetQueryable<T>(bool trackChanges = true, bool disabled = false)
            where T : class, new();
        IQueryable<T> GetQueryable<T>(Expression<Func<T, bool>> expression, bool trackChanges = true, bool disabled = false, bool deleted = false)
            where T : class, new();
        T Insert<T>(T item) where T : class, new();
        T Update<T>(T item) where T : class, new();
        int Save(string userId = "");

        T Delete<T>(T item) where T : class, new();
    }
}
