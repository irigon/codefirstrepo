﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDataContracts
{
    public class ExpenseDto
    {
        public decimal Amount { get; set; }
        public string CategoryName { get; set; }
        public int Month { get; set; }
    }
}
