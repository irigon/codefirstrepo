﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDataContracts
{
    public class ExpenseGroupDto
    {
        public string CategoryName { get; set; }
        public string Month { get; set; }
        public IEnumerable<ExpenseDto> Expenses { get; set; }
    }
}
