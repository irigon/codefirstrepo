﻿using AppDataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceContracts
{
    public interface IExpenseService
    {
        void AddExpense(AddExpenseDto model);
        IEnumerable<ExpenseGroupDto> List();
        IEnumerable<CategoryDto> GetCategoryList();
    }
}
