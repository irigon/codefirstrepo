﻿using AppDataContracts;
using AutoMapper;
using DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseService.Mapping
{
    public class EntityMappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<DbExpense, ExpenseDto>()
                .ForMember(a => a.CategoryName, b => b.MapFrom(c => c.Category.Name))
                .ForMember(a => a.Month, b => b.MapFrom(c => c.AddedDate.Month));
            CreateMap<DbCategory, CategoryDto>();
        }
    }
}
