﻿using ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppDataContracts;
using AppDataContracts.Core;
using DataContracts;
using Microsoft.AspNet.Identity;
using AutoMapper;
using AppDataContracts.Core.Common.Helpers;
using System.Globalization;

namespace ExpenseService
{
    public class Service : IExpenseService
    {
        private readonly IPrincipalService _principalService;
        private readonly IDataContext _dataContext;
        private readonly IMapper _mapper;
        public Service(
            IPrincipalService principalService,
            IDataContext dataContext,
            IMapper mapper)
        {
            _principalService = principalService;
            _dataContext = dataContext;
            _mapper = mapper;

        }
        public void AddExpense(AddExpenseDto model)
        {
            var userId = _principalService.GetPrincipal().Identity.GetUserId();
            var entity = new DbExpense()
            {
                AddedDate = DateTime.Now,
                Amount = model.Amount,
                CategoryId = model.CategoryId,
                UserId = userId
            };
            _dataContext.Insert(entity);
            _dataContext.Save();
        }

        public IEnumerable<ExpenseGroupDto> List()
        {
            var userId = _principalService.GetPrincipal().Identity.GetUserId();
            
            var result =  _dataContext.GetQueryable<DbExpense>().Where(a => a.UserId == userId).ConvertToList<DbExpense, ExpenseDto>(_mapper);
            return result.GroupBy(x => new { x.CategoryName, x.Month })
                .Select(y => new ExpenseGroupDto()
                {
                    CategoryName = y.Key.CategoryName,
                    Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(y.Key.Month),
                    Expenses = y.ToList()
                });
        }

        public IEnumerable<CategoryDto> GetCategoryList()
        {
            return _dataContext.GetQueryable<DbCategory>().ConvertToList<DbCategory, CategoryDto>(_mapper);
        }
    }
}
