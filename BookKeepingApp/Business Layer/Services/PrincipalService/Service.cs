﻿using ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace PrincipalService
{
    public class Service : IPrincipalService
    {
        public IPrincipal GetPrincipal()
        {
            if (HttpContext.Current == null || HttpContext.Current.User == null)
            {
                var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
                return identity;
            }
            else
            {
                return HttpContext.Current.User;
            }
        }
    }
}
