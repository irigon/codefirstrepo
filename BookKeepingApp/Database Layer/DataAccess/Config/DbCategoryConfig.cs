﻿using DataContracts;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Config
{
    public class DbCategoryConfig : EntityTypeConfiguration<DbCategory>
    {
        public DbCategoryConfig()
        {
            HasKey(a => a.Id);
            Property(a => a.Name).HasMaxLength(100);
        }
    }
}
