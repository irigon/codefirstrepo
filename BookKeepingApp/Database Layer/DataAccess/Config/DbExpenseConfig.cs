﻿using DataContracts;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Config
{
    public class DbExpenseConfig : EntityTypeConfiguration<DbExpense>
    {
        public DbExpenseConfig()
        {
            HasKey(a => a.Id);
            HasRequired(u => u.User)
                .WithMany(t => t.Expenses)
                .HasForeignKey(f => f.UserId);
            HasRequired(u => u.Category)
                .WithMany(t => t.Expenses)
                .HasForeignKey(f => f.CategoryId);
        }
    }
}
