﻿using DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class DBInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            base.Seed(context);
            var categories = new List<DbCategory>
            {
            new DbCategory{ Name = "food"},
            new DbCategory{ Name = "bills"},
            new DbCategory{ Name = "taxi"},
            new DbCategory{ Name = "entertainment"}
            };

            categories.ForEach(s => context.Categories.Add(s));
            context.SaveChanges();
        }
    }
}
