﻿using AppDataContracts.Core;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class DataContext : ApplicationDbContext, IDataContext
    {
        public IQueryable<T> GetQueryable<T>(bool trackChanges = true, bool disabled = false) where T : class, new()
        {
            return GetQueryable<T>(null, trackChanges, disabled);
        }

        public IQueryable<T> GetQueryable<T>(Expression<Func<T, bool>> expression, bool trackChanges = true, bool disabled = false, bool deleted = false) where T : class, new()
        {
            var query = GetQueryableNonAudit(expression, trackChanges, disabled, deleted);

            return query;
        }
        public IQueryable<T> GetQueryableNonAudit<T>(Expression<Func<T, bool>> expression, bool trackChanges = true, bool disabled = false, bool deleted = false) where T : class, new()
        {
            var query = trackChanges
                ? Set<T>().AsQueryable()
                : Set<T>().AsNoTracking();



            if (expression != null)
            {
                query = query.Where(expression);
            }

            return query;
        }

        public T Delete<T>(T item) where T : class, new()
        {
            //may do some intercepts
            //return AfterDelete(Set<T>().Remove(BeforeDelete(item)));
            return Set<T>().Remove(item);
        }

        public T Insert<T>(T item) where T : class, new()
        {
            //may do some intercepts
            //return AfterInsert(Set<T>().Add(BeforeInsert(item)));
            //return Set<T>().Add(item);

            return PerformAction(item, EntityState.Added);
        }

        public T Update<T>(T item) where T : class, new()
        {
            //may do some intercepts
            //return AfterInsert(Set<T>().Add(BeforeInsert(item)));
            //return Set<T>().Add(item);

            return PerformAction(item, EntityState.Modified);
        }

        //Save all changes and flush to DB
        public int Save(string userId = "")
        {
            int changes = base.SaveChanges();
            return changes;
        }


        public async Task SaveAsync()
        {
            await this.SaveChangesAsync();
        }
        protected virtual TItem PerformAction<TItem>(TItem item, EntityState entityState) where TItem : class, new()
        {
            Entry(item).State = entityState;
            return item;
        }
    }
}
