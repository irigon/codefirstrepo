namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExpenseTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DbExpenses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AddedDate = c.DateTime(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DbCategories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.CategoryId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DbExpenses", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.DbExpenses", "CategoryId", "dbo.DbCategories");
            DropIndex("dbo.DbExpenses", new[] { "UserId" });
            DropIndex("dbo.DbExpenses", new[] { "CategoryId" });
            DropTable("dbo.DbExpenses");
        }
    }
}
