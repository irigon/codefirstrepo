﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataContracts
{
    public class DbCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<DbExpense> Expenses { get; set; }
    }
}
