﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataContracts
{
    public class DbExpense
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public DateTime AddedDate { get; set; }
        public int CategoryId { get; set; }
        public virtual DbCategory Category { get; set; }
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
